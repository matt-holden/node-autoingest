/**
 * Author: Ismael Gorissen
 * Date: 10/08/13 12:47
 * Company: PinchProject
 */

var util = require('util');

function AbstractError(message, constr) {
    Error.captureStackTrace(this, constr || this);
    this.message = message || 'Error';
}

util.inherits(AbstractError, Error);

AbstractError.prototype.name = 'Abstract Error';

module.exports = AbstractError;