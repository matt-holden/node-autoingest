/**
 * Author: Ismael Gorissen
 * Date: 10/08/13 13:07
 * Company: PinchProject
 */

var util = require('util');

var AbstractError = require('./AbstractError.js');

util.inherits(InvalidFileSizeError, AbstractError);

function InvalidFileSizeError(message) {
    AbstractError.call(this, message, this.constructor);

    this.name = 'Invalid File Size Error';
}

module.exports = InvalidFileSizeError;