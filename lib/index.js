/**
 * Author: Ismael Gorissen
 * Date: 09/08/13 16:45
 * Company: PinchProject
 */

var fs = require('fs'),
    zlib = require('zlib'),
    path = require('path');

var request = require('request'),
    jsonschema = require('jsonschema'),
    async = require('async');

var Configurator = require('../modules/configurator.js'),
    InvalidParametersError = require('../errors/InvalidParametersError.js'),
    InvalidPathsError = require('../errors/InvalidPathsError.js'),
    InvalidFileSizeError = require('../errors/InvalidFileSizeError.js');

var parametersSchema = Configurator.loadSync('parameters_schema'),
    pathsSchema = Configurator.loadSync('paths_schema'),
    fileNamePrefix = Configurator.loadSync('filename_prefix'),
    validator = new jsonschema.Validator();

exports.INVALID_PARAMETERS_ERROR = InvalidParametersError;
exports.INVALID_PATHS_ERROR = InvalidPathsError;
exports.INVALID_FILE_SIZE_ERROR = InvalidFileSizeError;

/**
 *
 * @param parameters
 * @param paths
 * @param callback
 */
exports.downloadReportWith = function (parameters, paths, callback) {
    async.waterfall(
        [
            function (callback) {
                _validateAllJSON(parameters, paths, callback);
            },
            function (callback) {
                _createFileName(parameters, callback);
            },
            function (filename, callback) {
                _downloadReportArchive(filename, parameters, paths, callback);
            },
            function (filename, callback) {
                _extractReportArchive(filename, paths, callback);
            }
        ],
        function (err) {
            if (err) return callback(err);

            callback(null, paths);
        }
    );
};

/**
 *
 * @param parameters
 * @param paths
 * @param callback
 * @returns {*}
 * @private
 */
function _validateAllJSON(parameters, paths, callback) {
    var parametersErrors = _validateJSON(parameters, parametersSchema),
        pathsErrors = _validateJSON(paths, pathsSchema),
        err;

    if (parametersErrors) {
        err = new InvalidParametersError('Please enter all the required parameters. For help, please download the latest User Guide from the Sales and Trends module in iTunes Connect.');
        err.details = parametersErrors;

        return callback(err);
    }

    if (pathsErrors) {
        err = new InvalidPathsError('Please enter all the required path parameters.');
        err.details = pathsErrors;

        return callback(err);
    }

    return callback();
}

/**
 *
 * @param data
 * @param schema
 * @returns {*}
 * @private
 */
function _validateJSON(data, schema) {
    var result = validator.validate(data, schema);

    if (result.errors.length > 0) return result.errors;

    return null;
}

/**
 *
 * @param parameters
 * @param callback
 * @returns {*}
 * @private
 */
function _createFileName(parameters, callback) {
    var prefixArray = [parameters.report_type, parameters.report_subtype, parameters.date_type],
        prefixString = prefixArray.join('_');

    if (!fileNamePrefix[prefixString]) return callback(new InvalidParametersError());

    var filename = fileNamePrefix[prefixString] + '_' + parameters.vendor_number + '_' + parameters.report_date + '.txt';

    callback(null, filename);
}

/**
 *
 * @param filename
 * @param parameters
 * @param paths
 * @param callback
 * @private
 */
function _downloadReportArchive(filename, parameters, paths, callback) {
    paths.archive = path.join(paths.archive, filename + '.gz');

    var reportArchiveStream = fs.createWriteStream(paths.archive);

    request
        .post('https://reportingitc.apple.com/autoingestion.tft')
        .form({
            USERNAME: parameters.username,
            PASSWORD: parameters.password,
            VNDNUMBER: parameters.vendor_number,
            TYPEOFREPORT: parameters.report_type,
            DATETYPE: parameters.date_type,
            REPORTTYPE: parameters.report_subtype,
            REPORTDATE: parameters.report_date
        })
        .pipe(reportArchiveStream);

    reportArchiveStream.on('error', callback);

    reportArchiveStream.on(
        'finish',
        function () {
            _isArchiveFileEmpty(
                paths.archive,
                function (err) {
                    if (err) return callback(err);

                    callback(null, filename);
                }
            );
        }
    );
}

/**
 *
 * @param archivePath
 * @param callback
 * @private
 */
function _isArchiveFileEmpty(archivePath, callback) {
    fs.stat(
        archivePath,
        function (err, stats) {
            if (err) return callback(err);

            if (stats.size == 0) {
                fs.unlink(archivePath);
                return callback(new InvalidFileSizeError('The report you requested is not available at this time.  Please try again in a few minutes.'));
            }

            callback();
        }
    );
}

/**
 *
 * @param filename
 * @param paths
 * @param callback
 * @private
 */
function _extractReportArchive(filename, paths, callback) {
    paths.report = path.join(paths.report, filename);

    var reportStream = fs.createWriteStream(paths.report);

    zlib.gunzip(
        fs.readFileSync(paths.archive),
        function (err, data) {
            if (err) return callback(err);

            reportStream.end(data.toString(), 'utf8');
        }
    );

    reportStream.on('error', callback);
    reportStream.on('finish', callback);
}
